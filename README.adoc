= Documentation Asciidoctor en français

Traduction française de la documentation d'AsciiDoctor `.adoc`


== État

Pour l'instant seule la référence rapide (_quick-reference_) est traduite : https://framagit.org/Siltaar/asciidoctor-docs-fr/blob/ef8ac6360212d9111297ad6ed3d57850632a4b19/docs/asciidoc-syntax-quick-reference.fr.adoc[*Référence rapide de syntaxe Asciidoc*]


L'équipe _upstream_ https://github.com/asciidoctor/asciidoctor.org/pull/806[tarde à intégrer] la traduction dans le site web officiel, alors la voici déjà en ligne quelque part.

- `_includes/ex-footnote.adoc` : n'est pas à jour


== Contributions

Les contributions sont bienvenues.

Tous les fichiers qui n'ont pas l'extension `.fr.adoc` sont encore à traduire.

Les fichiers originaux ici sont peut-être en retard par rapport aux officiels sur : https://github.com/asciidoctor/asciidoctor.org

Des _merge requests_ pour les mettres à jour, ainsi que leurs traductions sont bienvenues aussi.

== Coup de pouce

Si vous appréciez ce travail, n'hésitez pas à le gratifier d'un petit coup de pouce via :

- https://patreon.com/metapress[Patreon]
- https://liberapay.com/Meta-Press.es[Liberapay]
- Bitcoins : bc1qpkj7cu33e5y4jxjuml3e799jv6kxra729c4u03c9vyk85cz6379swqdzs3
- ou Ethereums : 0x89f5f205141fc8eE6540e96BF2DCec20190c584
